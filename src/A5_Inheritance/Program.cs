﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace A5_Inheritance
{
    public class Program
    {
        public void Main(string[] args)
        {
            Console.WriteLine("Title: A5 - Inheritance and...\n"
                + "Overloading (Compile-Time Polymorphism) and Overriding (Run-Time Polymorphism)");
            Console.WriteLine("Author: Casey B. Hargarther");
            DateTime rightNow = DateTime.Now;
            string format = "ddd M/d/yy h:mm:ss t";
            Console.Write("Now: ");
            Console.WriteLine(rightNow.ToString(format));

            Product product1 = new Product();

            Console.WriteLine("product1 - (default constructor), using non-overloaded GetObjetInfo():");
            Console.WriteLine(product1.GetObjectInfo());
            
            // assigning values w/o using constructor
            product1.Code = "IT101";
            product1.Description = "Introduction to IT";
            product1.Price = 59.95m;

            Console.WriteLine();

            Console.WriteLine("product1 - (passing (literal) arg to overloaded GetObjectInfo(arg),\n" +
              "(after setting properties with literal values):");
            Console.WriteLine(product1.GetObjectInfo(", "));
            
            Console.WriteLine("\nUser Input: Call parameterized base constructor (accepts arguments):");

            //initalize arguments
            string p_code = "";
            string p_description = "";
            decimal p_price = 0.0m;
            string p_separator = "";

            //Capture User input

            Console.Write("Code (alphanumeric): ");
            p_code = Console.ReadLine();

            Console.Write("Description (alphanumeric): ");
            p_description = Console.ReadLine();

            Console.Write("Price: ");
            while (!decimal.TryParse(Console.ReadLine(), out p_price))
            {
                Console.WriteLine("Price must be numeric");
                Console.Write("Price: ");
            }

            Console.WriteLine();

            Console.WriteLine("\nproduct2 - (instantiating new object, passing *only 1st arg*)\n" +
                "(Demos why have constructor with default parameter values):");
            Console.WriteLine("*Note*: only 1st arg passed to constructor, others are default values.");
            Product product2 = new Product(p_code);

            Console.WriteLine("\nUser input: product2 - passing arg to overloaded GetObjectInfo(arg):");
            Console.Write("Delimiter (, : ;): ");
            p_separator = Console.ReadLine();
            Console.WriteLine(product2.GetObjectInfo(p_separator));

            Console.WriteLine("\nproduct3 - (instantiating new object, passing *all* args):");
            Product product3 = new Product(p_code, p_description, p_price);
            Console.WriteLine("\nUser input: product3 - passing arg to overloaded GetObjectInfo(arg)");
            Console.Write("Delimiter (, : ;): ");
            p_separator = Console.ReadLine();
            Console.WriteLine(product3.GetObjectInfo(p_separator));

            Console.WriteLine("\n Demonstrating Polymorphism (new derived object):");
            Console.WriteLine("(User input: Calling parameterized base class constructor explicitly.)\n");

            //initalize arguments
            string p_author = "";

            //Capture user input

            Console.Write("Code (alphanumeric): ");
            p_code = Console.ReadLine();

            Console.Write("Description (alphanumeric): ");
            p_description = Console.ReadLine();

            Console.Write("Price: ");
            while (!decimal.TryParse(Console.ReadLine(), out p_price))
            {
                Console.WriteLine("Price must be numeric");
            }

            Console.Write("Author: (alpha): ");
            p_author = Console.ReadLine();

            Console.WriteLine("\nbook1 - (instantiating new derived object, passing *all* args):");
            Book book1 = new Book(p_code, p_description, p_price, p_author);

            Console.WriteLine("\nUser input: book1 - passing arg to overloaded/overridden GetObjectInfo(arg)");
            Console.Write("Delimiter (, : ;): ");

            p_separator = Console.ReadLine();
            Console.WriteLine(book1.GetObjectInfo(p_separator));

            Console.WriteLine("\nDemonstrating Polymorphism (new derived object):");
            Console.WriteLine("(User input: Calling parameterized base class constructor explicitly.)\n");

            string p_version = "";

            Console.Write("Code (alphanumeric): ");
            p_code = Console.ReadLine();

            Console.Write("Description (alphanumeric): ");
            p_description = Console.ReadLine();

            Console.Write("Price: ");
            while (!decimal.TryParse(Console.ReadLine(), out p_price))
            {
                Console.WriteLine("Price must be numeric");

            }

            Console.Write("User input: version (ex. 0.0.0 or 1.2-rc3): ");
            p_version = Console.ReadLine();

            Console.WriteLine("\nsoftware1 - (instantiating new derived object, passing *all* args):");
            Software software1 = new Software(p_code, p_description, p_price, p_version);

            Console.WriteLine("\nUser input: software1 - passing arg to overloaded/overridden GetObjectInfo(arg)");
            Console.Write("Delimiter (, : ;): ");

            p_separator = Console.ReadLine();
            Console.WriteLine(software1.GetObjectInfo(p_separator));
            Console.WriteLine();
            Console.WriteLine("Press enter to exit ...");

            Console.Read();
        }
    }
}
