﻿namespace A5_Inheritance
{
    using System;
    public class Software : Product
    { 
    
        private string version;

        public string Version
        {
            get
            {
                return version;
            }
            set
            {
                version = value;
            }
        }

        public Software()
        {
            Version = "0.0.0 ";

            Console.WriteLine("\nCreating derived object from default constructor (accepts no arguments):");
        }

        public Software( string c = "SW1 ", string d = "Software default description ", decimal p = 3.00m, string v = "Default Version ") : base(c ,d ,p)
           
        {
            Version = v;
            Console.WriteLine("\nCreating derived object from parameterized constructor (accepts arguments):");

        }
        public override string GetObjectInfo(string sep)
        {
            return base.GetObjectInfo(sep) + sep + Version;
        }
    }
}