﻿namespace A5_Inheritance
{
    using System;

    public class Book : Product
    {
        private string author;

        public string Author
        {
            get
            {
                return author;
            }
            set
            {
                author = value;
            }

        }

    public Book()
    {
        this.author = "Default Author";
            Console.WriteLine("\nCreating derived object from default constructor (accepts no arguments) : ");
        }

       // private string Version;
        public Book(string c = "BK1", string d = "Book Default", decimal p = 2.00m, string a = "Default author") : base(c, d, p)
        {
            Author = a;

            Console.WriteLine("\nCreating derived object from parameterized constructor (accepts arguments) : ");

    }

        public override string GetObjectInfo(string sep)
        {
            return base.GetObjectInfo(sep) + sep + Author;
        }

}
}