﻿namespace A5_Inheritance
{
    using System;

    public class Product
    {
 
        private decimal price;

        public string Code { get; set; }

        public string Description { get; set; }


        public decimal Price
        {

            get { return price; }
            set { price = value; }
        }

      

        public Product()
        {
            Code = "ABC123";
            Description = "Default description ";
            Price = 0.0m;

            Console.WriteLine("\nCreating base object from default constructor (accepts no arguments) : ");

        }

    
        // private string Version;
        public Product(string c = " IT102 ",string d=" Intermediate IT ", decimal p = 1.00m) 
        {
            Code = c;
            Description = d;
            Price = p;

            Console.WriteLine("\nCreating base object from default constructor (accepts no arguments) : ");
    }


        public virtual string GetObjectInfo()
        {
            return Code + ", " + Price.ToString("c") + ", " + Description;

        }

        public virtual string GetObjectInfo(string sep)
        {
            return Code + sep + Price.ToString("c") + sep + Description;
        }

    }
}